---
# Source: gitlab-runner/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: gitlab-runner
  namespace: "gitlab-runner"
  labels:
    app: gitlab-runner
    chart: gitlab-runner-0.61.0
    release: "gitlab-runner"
    heritage: "Helm"
data:
  entrypoint: |
    #!/bin/bash
    set -e

    export CONFIG_PATH_FOR_INIT="/home/gitlab-runner/.gitlab-runner/"
    mkdir -p ${CONFIG_PATH_FOR_INIT}
    cp /configmaps/config.toml ${CONFIG_PATH_FOR_INIT}

    # Set up environment variables for cache
    if [[ -f /secrets/accesskey && -f /secrets/secretkey ]]; then
      export CACHE_S3_ACCESS_KEY=$(cat /secrets/accesskey)
      export CACHE_S3_SECRET_KEY=$(cat /secrets/secretkey)
    fi

    if [[ -f /secrets/gcs-applicaton-credentials-file ]]; then
      export GOOGLE_APPLICATION_CREDENTIALS="/secrets/gcs-applicaton-credentials-file"
    elif [[ -f /secrets/gcs-application-credentials-file ]]; then
      export GOOGLE_APPLICATION_CREDENTIALS="/secrets/gcs-application-credentials-file"
    else
      if [[ -f /secrets/gcs-access-id && -f /secrets/gcs-private-key ]]; then
        export CACHE_GCS_ACCESS_ID=$(cat /secrets/gcs-access-id)
        # echo -e used to make private key multiline (in google json auth key private key is oneline with \n)
        export CACHE_GCS_PRIVATE_KEY=$(echo -e $(cat /secrets/gcs-private-key))
      fi
    fi

    if [[ -f /secrets/azure-account-name && -f /secrets/azure-account-key ]]; then
      export CACHE_AZURE_ACCOUNT_NAME=$(cat /secrets/azure-account-name)
      export CACHE_AZURE_ACCOUNT_KEY=$(cat /secrets/azure-account-key)
    fi

    if [[ -f /secrets/runner-registration-token ]]; then
      export REGISTRATION_TOKEN=$(cat /secrets/runner-registration-token)
    fi

    if [[ -f /secrets/runner-token ]]; then
      export CI_SERVER_TOKEN=$(cat /secrets/runner-token)
    fi

    # Register the runner
    if ! sh /configmaps/register-the-runner; then
      exit 1
    fi

    # Run pre-entrypoint-script
    if ! bash /configmaps/pre-entrypoint-script; then
      exit 1
    fi

    # Start the runner
    exec /entrypoint run \
      --working-directory=/home/gitlab-runner

  config.toml: |
    shutdown_timeout = 0
    concurrent = 10
    check_interval = 30
    log_level = "info"

  
  config.template.toml:   |
    [[runners]]
      [runners.kubernetes]
        namespace = "gitlab-runner"
        image = "ubuntu:latest"
        privileged = false
  

  register-the-runner: |
    #!/bin/bash
    MAX_REGISTER_ATTEMPTS=30

    # Reset/unset the not needed flags when an authentication token
    RUN_UNTAGGED=""
    ACCESS_LEVEL=""

    for i in $(seq 1 "${MAX_REGISTER_ATTEMPTS}"); do
      echo "Registration attempt ${i} of ${MAX_REGISTER_ATTEMPTS}"
      /entrypoint register \
        ${RUN_UNTAGGED} \
        ${ACCESS_LEVEL} \
        --template-config /configmaps/config.template.toml \
        --non-interactive

      retval=$?

      if [ ${retval} = 0 ]; then
        break
      elif [ ${i} = ${MAX_REGISTER_ATTEMPTS} ]; then
        exit 1
      fi

      sleep 5
    done

    exit 0

  check-live: |
    #!/bin/bash
    set -eou pipefail

    if ! /usr/bin/pgrep -f ".*register-the-runner"  > /dev/null && ! /usr/bin/pgrep -f "gitlab.*runner"  > /dev/null ; then
      exit 1
    fi

    name=$(awk -F'"' '/^  name = ".*"/ { print $2 }' "${HOME%/root}/.gitlab-runner/config.toml")
    url=$(awk -F'"' '/^  url = ".*"/ { print $2 }' "${HOME%/root}/.gitlab-runner/config.toml")

    gitlab-runner verify -n "$name" -u "$url" 2>&1 | grep -E "is alive|is valid"

  pre-entrypoint-script: |
